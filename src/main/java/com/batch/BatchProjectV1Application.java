package com.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchProjectV1Application {

	public static void main(String[] args) {
		SpringApplication.run(BatchProjectV1Application.class, args);
	}
}
